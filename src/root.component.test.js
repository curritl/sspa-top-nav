import { render } from "@testing-library/react";
import Root from "./root.component";

describe("Root component", () => {
  it("should be in the document", () => {
    const { queryByTestId } = render(<Root />);
    expect(queryByTestId(/topnav/i)).toBeTruthy();
  });
});
